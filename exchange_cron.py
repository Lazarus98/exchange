from csv import DictWriter
from time import sleep
import exchange as ex
from datetime import datetime, timedelta

while True:
    # path = '/home/LazarusUA/app/exchange.csv'
    path = './exchange.csv'
    with open(path, 'a', newline='') as file:
        date = datetime.today() + timedelta(hours=2)
        fields = ('name', 'sale', 'buy', 'date')
        data = ex.exchange()
        writer = DictWriter(file, fieldnames=fields)
        # writer.writeheader()
        writer.writerow({'name': 'usd', 'sale': data['usd']['sale'], 'buy': data['usd']['buy'], 'date': date})
        writer.writerow({'name': 'eur', 'sale': data['eur']['sale'], 'buy': data['eur']['buy'], 'date': date})
        writer.writerow({'name': 'pln', 'sale': data['pln']['sale'], 'buy': data['pln']['buy'], 'date': date})
        print(date)
    sleep(60*60)
