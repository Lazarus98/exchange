from bs4 import BeautifulSoup as BS
from requests import get


def exchange():
    # proxies={'http': "proxy.server:3128"}
    page = get(url="https://minfin.com.ua/ua/currency/banks/").text
    page_data = BS(page, 'lxml').find(
        'table',
        class_='table-response mfm-table mfcur-table-lg-banks mfcur-table-lg ') \
        .find('tbody').find_all('tr')
    data = [x.find('td', class_='mfm-text-nowrap').text.split('\n') for
            x in page_data]

    kurs = {'usd': {'buy': data[0][1],
                    'sale': data[0][-2]},
            'eur': {'buy': data[1][1],
                    'sale': data[1][-2]},
            'pln': {'buy': data[3][1],
                    'sale': data[3][-2]}
            }

    return kurs
